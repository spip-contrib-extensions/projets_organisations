<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'projets_organisations_description' => '',
	'projets_organisations_nom' => 'Liens entre Projets et Organisations',
	'projets_organisations_slogan' => 'Permettre les liens entre organisations et projets',

);

?>